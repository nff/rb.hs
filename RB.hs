module Main where

-- ported from Qi/Shen at http://jng.imagine27.com/articles/2011-06-28-141124_purely_functional_types_red_black_trees_in_qi.html

type RBTreeNode a = (Int, a)

data Color = Red | Black deriving Show

data RBTree a = RBTree {
    color :: Color,
    left  :: RBTree a,
    node  :: RBTreeNode a,
    right :: RBTree a
} | RBTreeNil deriving Show

nodeKey :: RBTreeNode a -> Int
nodeKey (k, _) = k

makeTreeBlack :: RBTree a -> RBTree a
makeTreeBlack t = t {color = Black }

treeMember :: RBTreeNode a -> RBTree a -> Bool
treeMember x RBTreeNil = False
treeMember x (RBTree c a y b) = if nodeKey x < nodeKey y then
    treeMember x a
    else if nodeKey y < nodeKey x then
        treeMember x b
        else True

treeBalance :: RBTree a -> RBTree a
treeBalance (RBTree Black (RBTree Red (RBTree Red a x b) y c) z d) = (RBTree Red (RBTree Black a x b) y (RBTree Black c z d))
treeBalance (RBTree Black (RBTree Red a x (RBTree Red b y c)) z d) = (RBTree Red (RBTree Black a x b) y (RBTree Black c z d))
treeBalance (RBTree Black a x (RBTree Red (RBTree Red b y c) z d)) = (RBTree Red (RBTree Black a x b) y (RBTree Black c z d))
treeBalance (RBTree Black a x (RBTree Red b y (RBTree Red c z d))) = (RBTree Red (RBTree Black a x b) y (RBTree Black c z d))
treeBalance s = s

treeInsertNode :: RBTreeNode a -> RBTree a -> RBTree a
treeInsertNode x RBTreeNil = RBTree Red RBTreeNil x RBTreeNil
treeInsertNode x (RBTree c a y b) = if nodeKey x < nodeKey y then
    treeBalance $ RBTree c (treeInsertNode x a) y b
    else if nodeKey y < nodeKey x then
        treeBalance $ RBTree c a y $ treeInsertNode x b
        else RBTree c a y b

treeInsert :: RBTreeNode a -> RBTree a -> RBTree a
treeInsert x s = makeTreeBlack $ treeInsertNode x s

makeNode i = (i, "val-" ++ show i)

main :: IO ()
main = do
    let t = foldr (treeInsert . makeNode) RBTreeNil [1..10000]

    putStrLn $ "search!"

    putStrLn $ show $ treeMember (makeNode 10000) t
    putStrLn $ show $ treeMember (makeNode 10001) t

    return ()
